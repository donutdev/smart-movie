import { Injectable } from '@angular/core';
import { Result } from './interface/interface';

@Injectable({
  providedIn: 'root',
})
export class LocalService {
  constructor() {}

  addBasket(data: Result[]) {
    localStorage.setItem('basket', JSON.stringify(data));
  }

  removeMovie() {
    localStorage.removeItem('basket');
  }

  // getLocal
  getBasket(): Result[] {
    const data = localStorage.getItem('basket');
    if (!data) {
      return [];
    }
    // const res: resBasket = JSON.parse(data);
    const res = JSON.parse(data);
    return res;
  }
}
