import { Injectable } from '@angular/core';
import { Result } from './interface/interface';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  public basket: Result[] = [];
  public totalPrice: totalPriceI = {
    total: 0,
    unit: 0,
  };
  constructor() {}

  // setLocal
  setListbastket(data: Result) {
    this.basket.push(data);
  }
  // removeLocal
  removeListbasket() {
    this.basket = [];
  }
  // getLocal
  getListbasket(): Result[] {
    return this.basket;
  }

  setTotalPrice(data: totalPriceI) {
    this.totalPrice = data;
  }

  getTotalPrice(): totalPriceI {
    return this.totalPrice;
  }

  // ----------------------------------------------------------

  imageError() {
    return 'https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg';
  }
}

export interface totalPriceI {
  total: number;
  unit: number;
}
