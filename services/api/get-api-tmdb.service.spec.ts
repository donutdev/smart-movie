import { TestBed } from '@angular/core/testing';

import { GetApiTmdbService } from './get-api-tmdb.service';

describe('GetApiTmdbService', () => {
  let service: GetApiTmdbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetApiTmdbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
