import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment.prod';
import { Observable } from 'rxjs';
import { ResDataMovie } from 'services/interface/interface';

@Injectable({
  providedIn: 'root',
})
export class GetApiTmdbService {
  constructor(private http: HttpClient) {}

  getData(key: string): Observable<ResDataMovie> {
    return this.http.get<ResDataMovie>(
      `${environment.Url}/3/search/movie?api_key=${key}&query=a`
    );
  }
}
