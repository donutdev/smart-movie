import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppMainComponent } from './app-main.component';

const routes: Routes = [
  {
    path: '',
    component: AppMainComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../../page/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: 'detail',
        loadChildren: () =>
          import('../../page/detail/detail.module').then((m) => m.DetailModule),
      },
      {
        path: 'pricesubmit',
        loadChildren: () =>
          import('../../page/pricesubmit/pricesubmit.module').then(
            (m) => m.PricesubmitModule
          ),
      },
    ],
  },

  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppMainRoutingModule {}
