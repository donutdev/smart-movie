import { Component, OnInit } from '@angular/core';
import { GetApiTmdbService } from 'services/api/get-api-tmdb.service';

@Component({
  selector: 'app-app-main',
  templateUrl: './app-main.component.html',
  styleUrls: ['./app-main.component.css'],
})
export class AppMainComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {

  }
}
