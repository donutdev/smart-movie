import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppMainRoutingModule } from './app-main-routing.module';
import { AppMainComponent } from './app-main.component';
import { AppHerderComponent } from 'src/app/component/app-herder/app-herder.component';


@NgModule({
  declarations: [
    AppMainComponent,
    AppHerderComponent
  ],
  imports: [
    CommonModule,
    AppMainRoutingModule
  ]
})
export class AppMainModule { }
