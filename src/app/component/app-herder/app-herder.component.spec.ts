import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppHerderComponent } from "./AppHerderComponent";

describe('AppHerderComponent', () => {
  let component: AppHerderComponent;
  let fixture: ComponentFixture<AppHerderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppHerderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppHerderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
