import { Component, OnInit } from '@angular/core';
import { Result } from 'services/interface/interface';
import { LocalService } from 'services/local.service';

@Component({
  selector: 'app-app-herder',
  templateUrl: './app-herder.component.html',
  styleUrls: ['./app-herder.component.css'],
})
export class AppHerderComponent implements OnInit {
  public basket: Result[] = [];
  public unit = 0;
  constructor(private localService: LocalService) {}

  ngOnInit(): void {
    this.basket = this.localService.getBasket();
    this.unit = this.basket.length;
    console.log('localService', this.localService.getBasket().length);

    // this.localService.getBasket();
  }

  cal() {
    let total = 0;
    this.basket.forEach((item) => {
      total += item.price;
    });
    return total;
  }
}
