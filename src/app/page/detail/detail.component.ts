import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GetApiTmdbService } from 'services/api/get-api-tmdb.service';
import { AppService } from 'services/app.service';
import { Result } from 'services/interface/interface';
import { LocalService } from 'services/local.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
})
export class DetailComponent implements OnInit {
  public listMovie: Result[] = [];
  public addBaskets: Result[] = [];
  public bindingPrice = 0;
  public addPrice: Result[] = [];

  constructor(
    private getApiTmdbService: GetApiTmdbService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private localService: LocalService,
    private appService: AppService
  ) {}

  async ngOnInit(): Promise<void> {
    this.getData();
  }

  async getData() {
    const dataLocal = this.localService.getBasket();
    await this.activeRoute.queryParams.subscribe(async (params) => {
      const paramsId = params['page'];
      const apiKey = 'b34320383bc9cdd2054822c47f88a3b4';
      console.log('paramsId', paramsId);
      await this.getApiTmdbService.getData(apiKey).subscribe(
        (res) => {
          for (const data of res.results) {
            if (data.id === Number(paramsId)) {
              if (dataLocal) {
                for (const dataL of dataLocal) {
                  if (data.id === dataL.id) {
                    console.log('dataLocal-_>', dataLocal);
                    this.addBaskets.push(dataL);
                  }
                }
              }
              this.listMovie.push(data);
              console.log('data', this.listMovie);
            }
          }
        },
        (err) => {
          console.log(err);
        }
      );
    });
  }

  onKey(p: any) {
    const price = p.target.value;
    if (price) {
      this.bindingPrice = price;
    } else {
      this.bindingPrice = 0;
    }
  }

  async addBasket() {
    if (this.bindingPrice === 0) {
      Swal.fire({
        icon: 'error',
        title: 'กรุณากรอกราคาก่อนเพิ่มลงตะกร้า',
        text: 'กรุณากรอกข้อมูลให้ครบถ้วนก่อนกดบันทึก',
      });
      return;
    } else {
      for (const data of this.listMovie) {
        Object.assign(data, { price: Number(this.bindingPrice) });
        this.appService.setListbastket(data);
        await this.router.navigate(['/dashboard']);
      }
    }
  }
}
