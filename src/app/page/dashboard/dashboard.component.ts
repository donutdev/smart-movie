import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GetApiTmdbService } from 'services/api/get-api-tmdb.service';
import { AppService, totalPriceI } from 'services/app.service';
import { Result } from 'services/interface/interface';
import { LocalService } from 'services/local.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  public listMovie: Result[] = [];
  public searchText: string = '';
  public errImg: string = 'assets/imgs/images.png';
  public basket: Result[] = [];
  public unit = 0;
  public totalPrice = 0;
  constructor(
    private getApiTmdbService: GetApiTmdbService,
    private router: Router,
    private appService: AppService,
    private localService: LocalService
  ) {}

  ngOnInit(): void {
    this.getData();
    const listBasket = this.appService.getListbasket();
    if (listBasket && listBasket.length > 0) {
      this.localService.addBasket(listBasket);
    }
    this.basket = this.localService.getBasket();
    this.unit = this.basket.length;
  }

  clanBasket() {
    this.basket = this.localService.getBasket();
    this.unit = this.basket.length;
    if (this.unit > 0) {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger',
        },
        buttonsStyling: false,
      });

      swalWithBootstrapButtons
        .fire({
          title: 'ต้องการลบรายการทั้งหมดหรือไม่?',
          text: 'กรุณาตรวจสอบความถูกต้องก่อนทำการบันทึก!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'ใช่ ลบเลย!',
          cancelButtonText: 'ไม่ใช่',
          reverseButtons: true,
        })
        .then((result) => {
          if (result.isConfirmed) {
            this.localService.removeMovie();
            this.basket = this.localService.getBasket();
            this.unit = this.basket.length;
            const clanBasket = this.appService.getListbasket();
            if (clanBasket && clanBasket.length > 0) {
              this.appService.removeListbasket();
            }

            swalWithBootstrapButtons.fire(
              'บันทึกข้อมูลสำเร็จ!',
              'ทำการบันทึกข้อมูลสำเร็จ!',
              'success'
            );
          } else if (result.dismiss === Swal.DismissReason.cancel) {
            swalWithBootstrapButtons.fire(
              'บันทึกไม่สำเร็จ',
              'โปรดทำการบันทึกใหม่อีกครั้ง',
              'error'
            );
          }
        });
    }
  }
  cal() {
    let total = 0;
    this.basket.forEach((item) => {
      total += item.price;
    });
    if (this.unit >= 3) {
      total = total - (total * 10) / 100;
    }
    if (this.unit >= 5) {
      total = total - (total * 20) / 100;
    }
    this.totalPrice = total;
    return total;
  }
  getData() {
    const apiKey = 'b34320383bc9cdd2054822c47f88a3b4';
    this.getApiTmdbService.getData(apiKey).subscribe(
      (res) => {
        this.listMovie = res.results;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  searchMovie(event: any) {
    if (event.target.value === '' || event.target.value === null) {
      this.getData();
    } else {
      this.listMovie = this.listMovie.filter((res) =>
        res.title.toLowerCase().includes(event.target.value.toLowerCase())
      );
      console.log('event', event);
    }
  }

  selectMovie(id: number) {
    const dataMovie = this.listMovie.find((item) => item.id === id);

    this.router.navigate(['/detail', dataMovie], {
      queryParams: { page: dataMovie?.id },
    });
  }
  async submit() {
    if (this.unit > 0) {
      const totalPrice: totalPriceI = {
        total: this.totalPrice,
        unit: this.unit,
      };
      this.appService.setTotalPrice(totalPrice);
      await this.router.navigate(['/pricesubmit']);
    } else {
      Swal.fire({
        icon: 'question',
        title: 'ไม่พบรายการ',
        text: 'กรุณาเลือกภาพยนต์ที่ต้องการก่อนทำรายการ!',
      });
    }
  }
}
