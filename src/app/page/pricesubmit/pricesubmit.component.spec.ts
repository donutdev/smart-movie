import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PricesubmitComponent } from './pricesubmit.component';

describe('PricesubmitComponent', () => {
  let component: PricesubmitComponent;
  let fixture: ComponentFixture<PricesubmitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PricesubmitComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PricesubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
