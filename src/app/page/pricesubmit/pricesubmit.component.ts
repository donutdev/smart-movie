import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'services/app.service';
import { LocalService } from 'services/local.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pricesubmit',
  templateUrl: './pricesubmit.component.html',
  styleUrls: ['./pricesubmit.component.css'],
})
export class PricesubmitComponent implements OnInit {
  public basket: any[] = [];
  public priceS = 0;
  public unit = 0;
  public time = 0;
  public interval: any;
  constructor(
    private appService: AppService,
    private router: Router,
    private localService: LocalService
  ) {}

  ngOnInit(): void {
    this.getData();
    this.interval = null;
    this.cleanTime();
  }

  getData() {
    const basketLocal = localStorage.getItem('basket');
    this.basket = basketLocal ? JSON.parse(basketLocal) : [];
    this.unit = this.basket.length;
    console.log('basket', this.basket);
    const price = this.appService.getTotalPrice();
    this.priceS = price.total;
    console.log('price', price);
  }

  deleteMovie(id: string) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'ต้องการลบหรือไม่?',
        text: 'กรุณาตรวจสอบก่อนทำรายการ!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ใช่ ลบเลย!',
        cancelButtonText: 'ไม่ใช่',
        reverseButtons: true,
      })
      .then(async (result) => {
        if (result.isConfirmed) {
          const dataDelete = this.basket.find((item) => item.id === id);
          if (dataDelete) {
            const index = this.basket.indexOf(dataDelete);
            this.basket.splice(index, 1);
            const sortData = this.basket.sort((a: any, b: any) => {
              return a.index > b.index ? 1 : -1;
            });
            localStorage.setItem('basket', JSON.stringify(sortData));
            await this.getData();
          }
          // console.log('deleteMovie', dataDelete);
          swalWithBootstrapButtons.fire(
            'ลบสำเร็จ!',
            'ทำการลบข้อมูลสำเร็จ',
            'success'
          );
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            'ลบไม่สำเร็จ',
            'ทำการลบข้อมูลไม่สำเร็จ',
            'error'
          );
        }
      });
  }

  calPrice() {
    let total = 0;
    this.basket.forEach((item) => {
      total += item.price;
    });
    if (this.unit >= 3) {
      total = total - (total * 10) / 100;
    }
    if (this.unit >= 5) {
      total = total - (total * 20) / 100;
    }
    return total.toFixed(2);
  }

  submit() {
    this.cleanTime();
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'ต้องการส่งรายการหรือไม่?',
        text: 'กรุณาตรวจสอบก่อนทำรายการ!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ใช่ ส่งเลย!',
        cancelButtonText: 'ไม่ใช่',
        reverseButtons: true,
      })
      .then(async (result) => {
        this.localService.removeMovie();
        localStorage.removeItem('basket');
        if (result.isConfirmed) {
          this.localService.removeMovie();
          this.getData();
          const basketLocal = localStorage.getItem('basket');
          if (!basketLocal) {
            console.log('basketLocal', basketLocal);

            swalWithBootstrapButtons.fire(
              'สำเร็จ!',
              'ทำการชำระรายการสำเร็จ',
              'success'
            );
            // await this.router.navigate(['/']);
          }
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          swalWithBootstrapButtons.fire(
            'ส่งไม่สำเร็จ',
            'ทำการส่งรายการไม่สำเร็จ',
            'error'
          );
        }
      });
  }

  timePrice() {
    this.time = 60;
    this.interval = setInterval(() => {
      this.time -= 1;
      if (this.time === 0) {
        this.cleanTime();
        Swal.fire({
          icon: 'error',
          title: 'หมดเวลา',
          text: 'กรุณาทำรายการใหม่อีกครั่ง',
        });
      }
      console.log('this.time', this.time);
    }, 1000);
  }

  async cleanTime() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }
}
