import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PricesubmitRoutingModule } from './pricesubmit-routing.module';
import { PricesubmitComponent } from './pricesubmit.component';


@NgModule({
  declarations: [
    PricesubmitComponent
  ],
  imports: [
    CommonModule,
    PricesubmitRoutingModule
  ]
})
export class PricesubmitModule { }
