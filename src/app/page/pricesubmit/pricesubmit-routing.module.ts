import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PricesubmitComponent } from './pricesubmit.component';

const routes: Routes = [{ path: '', component: PricesubmitComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PricesubmitRoutingModule { }
